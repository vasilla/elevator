package controller;

import model.Elevator;
import model.ElevatorDistance;
import model.User;

import java.util.List;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {
        //This method is empty
    }

    public static int getElevator(User user, List<Elevator> elevators) {
        if (!elevators.isEmpty()) {
            int selectedElevator = 0;
            ElevatorDistance selectedElevatorDistance = elevators.get(0).getDistance(user);
            ElevatorDistance tempDistance;
            Elevator currentElevator;
            for (int i = 1; i < elevators.size(); i++) {
                currentElevator = elevators.get(i);
                if (currentElevator.isElevatorSuitable(user)) {
                    tempDistance = currentElevator.getDistance(user);
                    if (selectedElevatorDistance.checkIfOtherSlmaller(tempDistance)) {
                        selectedElevator = i;
                        selectedElevatorDistance = tempDistance;
                    }
                }
                else Logger.getAnonymousLogger().info("There not suitable elevators");
            }
            return selectedElevator;
        } else return -1;
    }



}
