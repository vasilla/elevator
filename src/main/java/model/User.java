package model;

import model.enums.UserMoveDirection;

public class User {

    private UserMoveDirection moveDirection;
    private int floorStart;

    public User() {
        floorStart = 1;
        moveDirection = UserMoveDirection.UP;
    }

    public User(UserMoveDirection moveDirection, int floorStart) {
        this.moveDirection = moveDirection;
        this.floorStart = floorStart;
    }

    public UserMoveDirection getMoveDirection() {
        return moveDirection;
    }

    public int getFloorStart() {
        return floorStart;
    }

    @Override
    public String toString() {
        return "User{}";
    }
}
