package model;

public class ElevatorBuildExeption extends Exception {

    public ElevatorBuildExeption(String message) {
        super(message);
    }
}
