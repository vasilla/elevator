package model.enums;

public enum ElevatorDirection {
    MOVE_UP,
    MOVE_DOWN,
    NOT_MOVE
}
