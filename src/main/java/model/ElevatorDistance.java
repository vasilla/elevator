package model;

public class ElevatorDistance {

    public static final int DISTANCE_PRIORITY = 1;
    public static final int MOVE_IN_USER_DIRECTION_PRIORITY = 2;
    public static final int SAME_DIRECTION_WITH_USER_PRIORITY = 3;

    private int distance;
    private int priority;

    ElevatorDistance(int distance, int priority) {
        this.distance = distance;
        this.priority = priority;
    }

    public boolean checkIfOtherSlmaller(ElevatorDistance other){
        return ((other.priority >priority)||((other.priority == this.priority)&&(other.distance<this.distance)));
    }


}
