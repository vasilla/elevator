package model;

import model.enums.ElevatorDirection;
import model.enums.UserMoveDirection;

import static model.enums.UserMoveDirection.DOWN;
import static model.enums.UserMoveDirection.UP;


public class Elevator {
    private String name;
    private int currentFloor;
    private int targetFloor;
    private int minFloor;
    private int maxFloor;

    public String getName() {
        return name;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public int getTargetFloor() {
        return targetFloor;
    }

    public Elevator() {

        this.currentFloor = 1;
        this.targetFloor = 1;
        this.minFloor = 1;
        this.maxFloor = 5;
    }



    public Elevator (Builder builder){
        this.currentFloor = builder.currentFloor;
        this.targetFloor = builder.targetFloor;
        this.minFloor = builder.minFloor;
        this.maxFloor = builder.maxFloor;
        this.name = builder.name;
    }

    public boolean isElevatorSuitable(User user){
        return  (!(((user.getFloorStart() == minFloor)&&(user.getMoveDirection()==UserMoveDirection.DOWN))||
                ((user.getFloorStart() == maxFloor)&&(user.getMoveDirection()== UP))||
                ((user.getFloorStart() > maxFloor)||(user.getFloorStart()<minFloor))));
    }

    public static class Builder{
        private String name;
        private int currentFloor;
        private int targetFloor;
        private int minFloor;
        private int maxFloor;

        public Builder(int minFloor, int maxFloor) {
            this.minFloor = minFloor;
            this.maxFloor = maxFloor;
            this.currentFloor = minFloor;
            this.targetFloor = minFloor;
            this.name = "unnamed";
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setCurrentFloor(int currentFloor) throws ElevatorBuildExeption{
            if ((currentFloor < minFloor)||(currentFloor > maxFloor)) throw new ElevatorBuildExeption("Wrong currentFloor.");
            this.currentFloor = currentFloor;
            return this;
        }

        public Builder setTargetFloor(int targetFloor)throws ElevatorBuildExeption {
            if ((targetFloor < minFloor)||(targetFloor > maxFloor)) throw new ElevatorBuildExeption("Wrong targetFloor.");
            this.targetFloor = targetFloor;
            return this;
        }
        public Elevator build(){
            return new Elevator(this);
        }
    }

    private ElevatorDirection getStatus(){
        if (currentFloor == targetFloor){
            return ElevatorDirection.NOT_MOVE;
        }
        if (targetFloor > currentFloor){
            return ElevatorDirection.MOVE_UP;
        }
        return ElevatorDirection.MOVE_DOWN;
    }

    public ElevatorDistance sameDirection(User user){
        int distance  = Math.abs(user.getFloorStart() - targetFloor);
        int priority = ElevatorDistance.DISTANCE_PRIORITY;
        if ((user.getFloorStart() == currentFloor)||
                ((user.getFloorStart() > currentFloor)^(user.getMoveDirection()== DOWN))) {
            priority = ElevatorDistance.MOVE_IN_USER_DIRECTION_PRIORITY;
            if ((user.getFloorStart() == targetFloor)||
                    ((user.getFloorStart() < targetFloor)^(user.getMoveDirection()== DOWN))) {
                distance = Math.abs(currentFloor - user.getFloorStart());
                priority = ElevatorDistance.SAME_DIRECTION_WITH_USER_PRIORITY;
            }
        }
        return new ElevatorDistance(distance,priority);
    }

    public ElevatorDistance oppositeDirection(User user){
        int distance  = Math.abs(user.getFloorStart() - targetFloor);
        int priority = ElevatorDistance.DISTANCE_PRIORITY;
        if ((user.getFloorStart() == targetFloor)||
                ((user.getFloorStart() < targetFloor)^(user.getMoveDirection()== DOWN))) {
            priority = ElevatorDistance.MOVE_IN_USER_DIRECTION_PRIORITY;
        }
        return new ElevatorDistance(distance,priority);
    }
    public ElevatorDistance getDistance(User user){
        ElevatorDirection elevatorDirection = getStatus();
        if (elevatorDirection == ElevatorDirection.NOT_MOVE){
            return new ElevatorDistance(Math.abs(user.getFloorStart() - targetFloor),ElevatorDistance.MOVE_IN_USER_DIRECTION_PRIORITY);
        }
        if (((elevatorDirection==ElevatorDirection.MOVE_UP)&&(user.getMoveDirection()==UP))||
                ((elevatorDirection==ElevatorDirection.MOVE_DOWN)&&(user.getMoveDirection()==DOWN))){
            return sameDirection(user);
        }
        if (((elevatorDirection==ElevatorDirection.MOVE_UP)&&(user.getMoveDirection()==DOWN))||
                ((elevatorDirection==ElevatorDirection.MOVE_DOWN)&&(user.getMoveDirection()==UP))){
            return oppositeDirection(user);
        }
        return new ElevatorDistance(Math.abs(user.getFloorStart() - targetFloor),ElevatorDistance.DISTANCE_PRIORITY);
    }

    @Override
    public String toString() {
        return String.format("Elevator: %s from %s to %s",name, currentFloor, targetFloor);
    }
}
