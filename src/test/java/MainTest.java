import controller.Main;
import model.Elevator;
import model.ElevatorBuildExeption;
import model.enums.UserMoveDirection;
import model.User;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class MainTest {
    private static final Logger log = Logger.getLogger(MainTest.class);
    private int testCount;
    private long time_mark;
    @BeforeSuite()
    public void init(){
        log.info("Test started  ---------------------------------------------");
        testCount = 1;
        time_mark = System.nanoTime();
    }
    @DataProvider(name = "testsOfficial")
    public Object[][] dataProviderForTest1(){
        Object[][] inputData = null;
        try {
            inputData = new Object[][]{
                {
                new User(UserMoveDirection.UP, 3),
                new ArrayList<Elevator>(Arrays.asList(
                        new Elevator.Builder(1, 100).setCurrentFloor(100).setTargetFloor(2).setName("#0").build(),
                        new Elevator.Builder(1, 100).setCurrentFloor(1).setTargetFloor(2).setName("#1").build()
                )),1},
                {
                new User(UserMoveDirection.DOWN, 4),
                new ArrayList<Elevator>(Arrays.asList(
                        new Elevator.Builder(1, 100).setCurrentFloor(100).setTargetFloor(2).setName("#0").build(),
                        new Elevator.Builder(1, 100).setCurrentFloor(5).setTargetFloor(3).setName("#1").build(),
                        new Elevator.Builder(1, 100).setCurrentFloor(7).setTargetFloor(4).setName("#2").build()
                )), 1},
                {
                new User(UserMoveDirection.DOWN, 4),
                new ArrayList<Elevator>(Arrays.asList(
                        new Elevator.Builder(1, 100).setCurrentFloor(100).setTargetFloor(2).setName("#0").build(),
                        new Elevator.Builder(1, 100).setCurrentFloor(5).setTargetFloor(4).setName("#1").build(),
                        new Elevator.Builder(1, 100).setCurrentFloor(2).setTargetFloor(4).setName("#2").build()
                )), 1},
                {
                new User(UserMoveDirection.DOWN, 96),
                new ArrayList<Elevator>(Arrays.asList(
                    new Elevator.Builder(1, 100).setCurrentFloor(100).setTargetFloor(99).setName("#0").build(),
                    new Elevator.Builder(1, 100).setCurrentFloor(2).setTargetFloor(96).setName("#1").build(),
                    new Elevator.Builder(1, 100).setCurrentFloor(95).setTargetFloor(94).setName("#2").build(),
                    new Elevator.Builder(1, 100).setCurrentFloor(10).setTargetFloor(1).setName("#3").build()
                )), 1},
                {
                new User(UserMoveDirection.DOWN, 5), new ArrayList<Elevator>(Arrays.asList(
                    new Elevator.Builder(1, 100).setCurrentFloor(2).setTargetFloor(2).setName("#0").build(),
                    new Elevator.Builder(1, 100).setCurrentFloor(6).setTargetFloor(3).setName("#1").build(),
                    new Elevator.Builder(1, 100).setCurrentFloor(4).setTargetFloor(2).setName("#2").build()
                )), 1},
                {
                new User(UserMoveDirection.UP, 5), new ArrayList<Elevator>(Arrays.asList(
                    new Elevator.Builder(1, 100).setCurrentFloor(7).setTargetFloor(9).setName("#0").build(),
                    new Elevator.Builder(1, 100).setCurrentFloor(3).setTargetFloor(4).setName("#1").build(),
                    new Elevator.Builder(1, 100).setCurrentFloor(10).setTargetFloor(5).setName("#2").build()
                )), 2},
                {
                new User(UserMoveDirection.UP, 5), new ArrayList<Elevator>(Arrays.asList(
                    new Elevator.Builder(1, 100).setCurrentFloor(7).setTargetFloor(9).setName("#0").build(),
                    new Elevator.Builder(1, 100).setCurrentFloor(6).setTargetFloor(8).setName("#1").build(),
                    new Elevator.Builder(1, 100).setCurrentFloor(2).setTargetFloor(3).setName("#2").build(),
                    new Elevator.Builder(1, 100).setCurrentFloor(4).setTargetFloor(3).setName("#3").build()
                )), 2}

            };
        }catch (ElevatorBuildExeption e){
            log.error("Error in test."+e.getMessage());
        }
        return  inputData;
    }

    @Test(dataProvider = "testsOfficial")
    public void test(User user,ArrayList<Elevator> list,int elevatorCorrectIndex){
        int elevatorIndex = Main.getElevator(user, list);
        Assert.assertNotEquals(elevatorIndex,-1);
        Assert.assertEquals(elevatorIndex,elevatorCorrectIndex);
        long timeWasted = System.nanoTime() - time_mark;
        StringBuilder elevators = new StringBuilder();
        for (Elevator temp:
             list) {
            elevators.append("\t"+temp.toString()+"\n");
        }
        log.info(String.format("Test #%d \n" +
                        "expected:%s \n" +
                        "found:%s\n" +
                        "passed:%s \n" +
                        "time wasted: %s nano seconds\n" +
                "User: floor:%d , direction %s \n" +
                "Elevators:\n" +
                "%s",testCount,elevatorCorrectIndex,elevatorIndex,(elevatorCorrectIndex == elevatorIndex),
                timeWasted,user.getFloorStart(),user.getMoveDirection().toString(),elevators.toString()));
        testCount++;
        time_mark = System.nanoTime();

   }

   @AfterTest
   public void checkTest(){
        log.info("Test done -----------------------------------------------------");
   }

}